#include "World/SQChunk.h"

#include "Core/SQGameInstance.h"
#include "Core/SQPlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Libraries/SQPerlinLibrary.h"
#include "Net/UnrealNetwork.h"
#include "World/SQWorldManager.h"
#include "World/Structs/SQCompressedBlocks.h"
#include "World/SQChunksManager.h"

ASQChunk::ASQChunk()
{
	PrimaryActorTick.bCanEverTick = false;

	bReplicates = true;
	bOnlyRelevantToOwner = true;
	NetUpdateFrequency = 0.0f;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	Mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
	Mesh->bUseAsyncCooking = true;

	const auto MaterialAsset = ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("Material'/Game/Materials/M_World.M_World'"));
	ChunkMat = MaterialAsset.Object;

	// const auto CurveAsset = ConstructorHelpers::FObjectFinder<UCurveFloat>(TEXT("CurveFloat'/Game/MapSystem/C_IslandPerlin.C_IslandPerlin'"));
	// PerlinCurve = CurveAsset.Object;
	
	BlockList.SetNum(ChunkSize.X * ChunkSize.Y * ChunkSize.Z);
}

void ASQChunk::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams Params;

	Params.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(ASQChunk,ChunkPos,Params);
}

void ASQChunk::BeginPlay()
{
	Super::BeginPlay();
	
	GameInstance = Cast<USQGameInstance>(GetWorld()->GetGameInstance());

	const auto PlayerController = Cast<ASQPlayerController>(GetOwner());
	
	if (GetNetMode() == NM_Client)
	{
		GEngine->AddOnScreenDebugMessage(-1,5,FColor::Red,*ChunkPos.ToString());
		PlayerController->ChunksManager->LoadedChunks.Add(ChunkPos,this);
	}
	
	PlayerController->ChunksManager->ChunksToUpdate.Enqueue(this);
}

void ASQChunk::OnRep_Owner()
{
	Super::OnRep_Owner();

	const auto PlayerController = Cast<ASQPlayerController>(GetOwner());
		
	PlayerController->ChunksManager->ChunksToUpdate.Enqueue(this);
}

void ASQChunk::PopulateChunk()
{
	bool bGenerateTower = false;
	FIntPoint TowerPos = FIntPoint::ZeroValue;

	for (const FIntPoint& TowerChunk : TowersChunks)
		if (ChunkPos == TowerChunk)
		{
			bGenerateTower = true;
			TowerPos = FIntPoint(
				FMath::RandRange(0,ChunkSize.X - 1),
				FMath::RandRange(0,ChunkSize.Y - 1)
			);
		}
	
	for (int32 x = 0; x < ChunkSize.X; ++x)
		for (int32 y = 0; y < ChunkSize.Y; ++y)
		{
			constexpr int32 HorizonHeight = 128;
			const float IsleSize =
				//WorldManager->RenderDistance
				20
				* ChunkSize.X / 2;
			const int32 GlobalX = x + ChunkPos.X * 16;
			const int32 GlobalY = y + ChunkPos.Y * 16;

			const float PerlinZ = USQPerlinLibrary::TwoD_Perlin_Fractal(GlobalX,GlobalY,1,30,10,2,20);

			const float DX = FMath::Abs(GlobalX) / IsleSize * 100;
			const float DY = FMath::Abs(GlobalY) / IsleSize * 100;

			const float CurveX = PerlinCurve->GetFloatValue(DX);
			const float CurveY = PerlinCurve->GetFloatValue(DY);

			const int32 MaxZ = FMath::RoundToInt(HorizonHeight + PerlinZ + CurveX + CurveY);
			const int32 MinZ = FMath::RoundToInt(HorizonHeight - PerlinZ * 2.5 - CurveX * 2.5 - CurveY * 2.5 - 1);
			

			const float ThreePerlin = USQPerlinLibrary::TwoD_Perlin_Noise(GlobalX,GlobalY,8,10);
			const float ThreePerlinPlacement = USQPerlinLibrary::TwoD_Perlin_Noise(GlobalX,GlobalY,2,20);
			
			for (int32 z = 0; z < ChunkSize.Z; ++z)
			{
				const int32 BlockIndex = GetBlockIndex(x,y,z);
				if (z >= MinZ && z <= MaxZ)
				{
					if (z == MaxZ)
					{
						const int32 DownBlock = GetBlockIndex(x,y,z-1);
						if (ThreePerlin > 0 && ThreePerlinPlacement > 8.5 && BlockList[DownBlock] == 3)
						{
							const int32 Structure = FMath::RandRange(0,3);
							SpawnStructure(FIntVector(x,y,z), Structure);
						}
						else BlockList[BlockIndex] = 0;
					}
					else if (z == MaxZ - 1) BlockList[BlockIndex] = 3;
					else if (z > MaxZ - 4) BlockList[BlockIndex] = 2;
					else BlockList[BlockIndex] = 1;
				}
				else
					BlockList[BlockIndex] = 0;
			}

			if (bGenerateTower && x == TowerPos.X && y == TowerPos.Y)
			{
				SpawnStructure(FIntVector(x,y,MaxZ - 4),4);
			}
		}
	bIsGenerated = true;
}

void ASQChunk::CreateMeshData()
{
	Vertices.Reset();
	Triangles.Reset();
	Normals.Reset();
	UV.Reset();
	Tangents.Reset();
	VertexColors.Reset();
	Masks.Reset();
	VertexCount = 0;
	
	// Sweep over each axis (X, Y, Z)
	for (int Axis = 0; Axis < 3; ++Axis)
	{
		// 2 Perpendicular axis
		const int Axis1 = (Axis + 1) % 3;
		const int Axis2 = (Axis + 2) % 3;

		const int MainAxisLimit = ChunkSize[Axis];
		const int Axis1Limit = ChunkSize[Axis1];
		const int Axis2Limit = ChunkSize[Axis2];

		auto DeltaAxis1 = FIntVector::ZeroValue;
		auto DeltaAxis2 = FIntVector::ZeroValue;

		auto ChunkItr = FIntVector::ZeroValue;
		auto AxisMask = FIntVector::ZeroValue;

		AxisMask[Axis] = 1;
		
		Masks.SetNum(Axis1Limit * Axis2Limit);

		// Check each slice of the chunk
		for (ChunkItr[Axis] = -1; ChunkItr[Axis] < MainAxisLimit;)
		{
			int N = 0;

			// Compute Mask
			for (ChunkItr[Axis2] = 0; ChunkItr[Axis2] < Axis2Limit; ++ChunkItr[Axis2])
			{
				for (ChunkItr[Axis1] = 0; ChunkItr[Axis1] < Axis1Limit; ++ChunkItr[Axis1])
				{
					const auto CurrentBlock = GetBlock(ChunkItr);
					const auto CompareBlock = GetBlock(ChunkItr + AxisMask);

					const bool CurrentBlockOpaque = CurrentBlock != 0;
					const bool CompareBlockOpaque = CompareBlock != 0;

					if (CurrentBlockOpaque == CompareBlockOpaque)
					{
						Masks[N++] = FSQMask{-1, 0};
					}
					else if (CurrentBlockOpaque)
					{
						Masks[N++] = FSQMask{CurrentBlock, 1};
					}
					else
					{
						Masks[N++] = FSQMask{CompareBlock, -1};
					}
				}
			}

			++ChunkItr[Axis];
			N = 0;

			// Generate Mesh From Mask
			for (int j = 0; j < Axis2Limit; ++j)
			{
				for (int i = 0; i < Axis1Limit;)
				{
					if (Masks[N].Normal != 0)
					{
						const auto CurrentMask = Masks[N];
						ChunkItr[Axis1] = i;
						ChunkItr[Axis2] = j;

						int Width;

						for (Width = 1; i + Width < Axis1Limit && CompareMask(Masks[N + Width], CurrentMask); ++Width)
						{
						}

						int Height;
						bool Done = false;

						for (Height = 1; j + Height < Axis2Limit; ++Height)
						{
							for (int k = 0; k < Width; ++k)
							{
								if (CompareMask(Masks[N + k + Height * Axis1Limit], CurrentMask)) continue;

								Done = true;
								break;
							}

							if (Done) break;
						}

						DeltaAxis1[Axis1] = Width;
						DeltaAxis2[Axis2] = Height;

						CreateQuad(
							CurrentMask, AxisMask,
							ChunkItr,
							ChunkItr + DeltaAxis1,
							ChunkItr + DeltaAxis2,
							ChunkItr + DeltaAxis1 + DeltaAxis2
						);

						DeltaAxis1 = FIntVector::ZeroValue;
						DeltaAxis2 = FIntVector::ZeroValue;

						for (int l = 0; l < Height; ++l)
						{
							for (int k = 0; k < Width; ++k)
							{
								Masks[N + k + l * Axis1Limit] = FSQMask{-1, 0};
							}
						}

						i += Width;
						N += Width;
					}
					else
					{
						i++;
						N++;
					}
				}
			}
		}
	}
}

void ASQChunk::IsAir()
{
	int NotAirBlocks = 0;
	for (int i = 0; i < BlockList.Num(); ++i)
	{
		if (BlockList[i] != 0)
		{
			NotAirBlocks++;
		}
	}
	UE_LOG(LogTemp,Warning,TEXT("Not Air Block = %i"),NotAirBlocks)
}

void ASQChunk::CreateQuad(
	const FSQMask Mask,
	const FIntVector& AxisMask,
	const FIntVector& V1,
	const FIntVector& V2,
	const FIntVector& V3,
	const FIntVector& V4
)
{
	const FVector3d Normal = FVector3d(AxisMask * Mask.Normal);

	Vertices.Append({
		FVector3d(V1) * 100,
		FVector3d(V2) * 100,
		FVector3d(V3) * 100,
		FVector3d(V4) * 100
	});

	Triangles.Append({
		VertexCount,
		VertexCount + 2 + Mask.Normal,
		VertexCount + 2 - Mask.Normal,
		VertexCount + 3,
		VertexCount + 1 - Mask.Normal,
		VertexCount + 1 + Mask.Normal
	});

	int32 DirectionOffset;
	if (Normal.X == 1)
		DirectionOffset = 0;
	
	else if (Normal.X == -1)
		DirectionOffset = 1;
	
	else if (Normal.Y == 1)
		DirectionOffset = 2;
	
	else if (Normal.Y == -1)
		DirectionOffset = 3;
	
	else if (Normal.Z == 1)
		DirectionOffset = 4;
	
	else
		DirectionOffset = 5;
	
	FSQBlock BlockData = GameInstance->Blocks[Mask.Block];
	const FColor VertexColor = FColor(BlockData.AtlasOffsetX[DirectionOffset],BlockData.AtlasOffsetY[DirectionOffset],0,1);
	VertexColors.Append({ VertexColor, VertexColor, VertexColor, VertexColor });

	Normals.Append({
		Normal,
		Normal,
		Normal,
		Normal
	});

	UV.Append({
		FVector2D(0, 0),
		FVector2D(0, 1),
		FVector2D(1, 0),
		FVector2D(1, 1)
	});

	VertexCount += 4;
}

int32 ASQChunk::GetBlock(const FIntVector Index) const
{	
	if (Index.X >= ChunkSize.X || Index.Y >= ChunkSize.Y || Index.Z >= ChunkSize.Z || Index.X < 0 || Index.Y < 0 || Index.Z < 0)
		return 0;
	return BlockList[GetBlockIndex(Index.X, Index.Y, Index.Z)];
}

bool ASQChunk::CompareMask(const FSQMask M1, const FSQMask M2)
{
	return M1.Block == M2.Block && M1.Normal == M2.Normal;
}

void ASQChunk::RenderChunk()
{
	AsyncTask(ENamedThreads::HighTaskPriority, [this]
	{		
		CreateMeshData();
			
		if (const auto PlayerController = Cast<ASQPlayerController>(
			UGameplayStatics::GetPlayerController(GetWorld(),0))
			)
		{
			PlayerController->ChunksManager->ChunksToShow.Enqueue(this);
		}
	});
}

void ASQChunk::ShowChunk() const
{	
	Mesh->CreateMeshSection(0,Vertices,Triangles,Normals,UV,VertexColors,Tangents,true);
	
	Mesh->SetMaterial(0,ChunkMat);
	
	Mesh->SetVisibility(true);
}

void ASQChunk::HideChunk() const
{
	Mesh->ClearMeshSection(0);

	Mesh->SetVisibility(false);
}

void ASQChunk::ModifyBlock(const FIntVector& BlockPos, const int32 NewBlock, int32& OldBlock)
{
	FSQBlock BlockData;
	GameInstance->GetBlock(NewBlock,BlockData);
	if (BlockData.bIsUsable)
		NewUtilityBlocks.Enqueue(FSQBlockToModify(BlockPos,NewBlock,true));
	const int32 BlockIndex = GetBlockIndex(BlockPos.X,BlockPos.Y,BlockPos.Z);
	OldBlock = BlockList[BlockIndex];
	BlockList[BlockIndex] = NewBlock;
	Multicast_ChunkUpdate();
}

bool ASQChunk::DamageBlock(const FIntVector& BlockPos, int32 Damage, int32& BlockDestroyed)
{
	const int32 BlockIndex = GetBlockIndex(BlockPos.X,BlockPos.Y,BlockPos.Z);
	bool bBlockDestroyed = false;

	if (BlockList[BlockIndex] > 0)
	{
		FSQBlock BlockData;
		GameInstance->GetBlock(BlockList[BlockIndex],BlockData);

		if (!HealthBlocks.Contains(BlockIndex))
			HealthBlocks.Add(BlockIndex,BlockData.MaxHealth);

		int32 BlockHealth = HealthBlocks.FindRef(BlockIndex);
		BlockHealth -= (Damage - BlockData.Hardness);

		if (BlockHealth <= 0)
		{
			BlockDestroyed = BlockList[BlockIndex];
			BlockList[BlockIndex] = 0;
			HealthBlocks.Remove(BlockIndex);
	
			if (ASQUtilityBlock* UtilityBlock = UtilitiesBlocks.FindRef(BlockPos))
				UtilityBlock->Destroy();
	
			Multicast_ChunkUpdate();

			bBlockDestroyed = true;
		}
		else
		{
			HealthBlocks.Add(BlockIndex,BlockHealth);
		}
	}
	
	return bBlockDestroyed;
}

void ASQChunk::SpawnStructure(FIntVector ThreePos, const int32 StructureID) const
{
	FSQWorldTemplate StructureData;
	GameInstance->GetStructure(StructureID,StructureData);
	for (FSQBlockToModify& BlockToModify : StructureData.BlockToModifies)
	{
		const FVector NewBlockPos = FVector(ThreePos + BlockToModify.BlockIndex + FIntVector(ChunkPos.X,ChunkPos.Y,0) * 16) * 100;
		WorldManager->AddBlockToModify(NewBlockPos, BlockToModify);
	}
}

bool ASQChunk::OutOfChunk(const FIntVector& BlockPos) const
{
	return (BlockPos.X >= ChunkSize.X || BlockPos.Y >= ChunkSize.Y || BlockPos.Z >= ChunkSize.Z || BlockPos.X < 0 || BlockPos.Y < 0 || BlockPos.Z < 0);
}

void ASQChunk::AddQueueToModify(TArray<FSQBlockToModify> BlockToModifies)
{
	for (const FSQBlockToModify& BlockToModify : BlockToModifies)
		BlocksToModify.Enqueue(BlockToModify);
}

void ASQChunk::AddBlocksToModify()
{
	FSQBlockToModify BlockToModify;
	while (BlocksToModify.Dequeue(BlockToModify))
	{
		const int32 BlockIndex = GetBlockIndex(BlockToModify.BlockIndex.X,BlockToModify.BlockIndex.Y,BlockToModify.BlockIndex.Z);

		if (BlockToModify.bIsImportant || !BlockToModify.bIsImportant && BlockList[BlockIndex] == 0)
			BlockList[BlockIndex] = BlockToModify.BlockType;
	}
	while (NewUtilityBlocks.Dequeue(BlockToModify))
	{
		FSQBlock BlockData;
		GameInstance->GetBlock(BlockToModify.BlockType,BlockData);
		ASQUtilityBlock* UtilityBlock = GWorld->SpawnActor<ASQUtilityBlock>
			(BlockData.UtilityBlockClass,GetActorLocation() + FVector(BlockToModify.BlockIndex * 100) + 50, FRotator(0));
		UtilitiesBlocks.Add(BlockToModify.BlockIndex,UtilityBlock);
	}
}

void ASQChunk::CompressChunk()
{
	TArray<FSQCompressedBlocks> CompressedBlocks = {};
	FSQCompressedBlocks ActualBlock = {BlockList[0],0};
	for (int32 i = 1; i < BlockList.Num(); i++)
	{
		if (ActualBlock.BlockID == BlockList[i])
		{
			ActualBlock.Length++;
		}
		else
		{
			CompressedBlocks.Add(ActualBlock);
			ActualBlock = {BlockList[i],0};
		}
	}
	
	CompressedBlocks.Add(ActualBlock);
	
	CompressedChunk = CompressedBlocks;
}

void ASQChunk::DecompressChunk()
{
	TArray<int32> Blocks = {};

	for (const FSQCompressedBlocks& CompressedBlocks : CompressedChunk)
	{
		TArray<int32> BlocksToAdd;
		BlocksToAdd.Init(CompressedBlocks.BlockID,CompressedBlocks.Length + 1);
		Blocks.Append(BlocksToAdd);
	}
	BlockList = Blocks;
}

void ASQChunk::Multicast_ChunkUpdate_Implementation()
{
	const auto PlayerController = Cast<ASQPlayerController>(
		UGameplayStatics::GetPlayerController(GetWorld(),0)
		);
	PlayerController->ChunksManager->ChunksToRender.Enqueue(this);
}

int32 ASQChunk::GetBlockIndex(const int X, const int Y, const int Z) const
{
	return Z * ChunkSize.X * ChunkSize.Y + Y * ChunkSize.X + X; 
}
