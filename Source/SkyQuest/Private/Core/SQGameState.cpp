// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/SQGameState.h"

#include "Net/UnrealNetwork.h"

ASQGameState::ASQGameState()
{
	NetUpdateFrequency = 0.0f;
}

void ASQGameState::BeginPlay()
{
	Super::BeginPlay();

	if (GetNetMode() == NM_ListenServer)
		Server_SpawnWorld();
}

void ASQGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	FDoRepLifetimeParams Params;

	Params.bIsPushBased = true;

	DOREPLIFETIME_WITH_PARAMS_FAST(ASQGameState, WorldManager, Params);
}

void ASQGameState::Server_SpawnWorld_Implementation()
{
	WorldManager = GetWorld()->SpawnActorDeferred<ASQWorldManager>(WorldManagerClass,FTransform(),this,
		nullptr,ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	
	WorldManager->FinishSpawning(FTransform());
}
