// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/SQPlayerController.h"

#include "Core/SQGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "World/SQVoxel.h"
#include "World/SQWorldManager.h"
#include "World/SQChunksManager.h"


ASQPlayerController::ASQPlayerController()
{
	ChunksManager = CreateDefaultSubobject<USQChunksManager>(TEXT("ChunksManager"));
}

void ASQPlayerController::BeginPlay()
{
	Super::BeginPlay();

	ChunksManager->PlayerController = this;
	
	const USQGameInstance* GameInstance = Cast<USQGameInstance>(GetWorld()->GetGameInstance());
	ChunksManager->RenderDistance = GameInstance->RenderDistance / 2;
}

void ASQPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (IsValid(GetPawn()))
	{
		const FIntPoint OccupiedChunk = USQVoxel::GetChunkIndices(GetPawn()->GetActorLocation());
		if (OccupiedChunk != ChunksManager->ActualChunk)
		{
			ChunksManager->ReRenderChunks(OccupiedChunk);
		}
	}
}

