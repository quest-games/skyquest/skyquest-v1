// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SQInventory.h"
#include "Components/ActorComponent.h"
#include "Structs/SQRecipe.h"
#include "SQCrafter.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SKYQUEST_API USQCrafter : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USQCrafter();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	UPROPERTY(BlueprintReadWrite)
	USQInventory* Inventory;

	UFUNCTION(BlueprintCallable)
	bool CanCreate(FSQRecipe Recipe);

	UFUNCTION(BlueprintCallable)
	USQItem* CraftItem(int32 RecipeID);
};
