// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Structs/SQItemData.h"
#include "SQItem.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class SKYQUEST_API USQItem : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	static USQItem* CreateItem(FSQItemData& Item, int32 Stack);
	
	UPROPERTY(BlueprintReadWrite)
	FSQItemData ItemData;

	UPROPERTY(BlueprintReadWrite)
	int32 Stack = 1;

	UFUNCTION(BlueprintCallable)
	bool IsFullStack();
};
