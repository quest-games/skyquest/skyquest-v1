// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SQGameMode.generated.h"

UCLASS(minimalapi)
class ASQGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASQGameMode();

	virtual void BeginPlay() override;
};



