#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SQPlayerController.generated.h"

class ASQWorldManager;
class USQChunksManager;

UCLASS()
class SKYQUEST_API ASQPlayerController : public APlayerController
{
	GENERATED_BODY()

	ASQPlayerController();
	
public:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	USQChunksManager* ChunksManager;

	UPROPERTY()
	ASQWorldManager* WorldManager;
};
