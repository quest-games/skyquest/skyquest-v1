// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "World/SQWorldManager.h"
#include "SQGameState.generated.h"

/**
 * 
 */
UCLASS()
class SKYQUEST_API ASQGameState : public AGameStateBase
{
	GENERATED_BODY()

	ASQGameState();

public:
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(Server, Reliable)
	void Server_SpawnWorld();

	UPROPERTY(Replicated)
	ASQWorldManager* WorldManager;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ASQWorldManager> WorldManagerClass;
};
