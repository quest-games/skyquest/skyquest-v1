﻿#pragma once

#include "SQBlockToModify.generated.h"

USTRUCT(BlueprintType)
struct SKYQUEST_API FSQBlockToModify
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FIntVector BlockIndex = FIntVector::ZeroValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 BlockType = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsImportant = false;

	FSQBlockToModify() { }
	
	FSQBlockToModify(const FIntVector _BlockIndex,const int32 _BlockType, const bool _bIsImportant)
	{
		BlockIndex = _BlockIndex;
		BlockType = _BlockType;
		bIsImportant = _bIsImportant;
	}
};