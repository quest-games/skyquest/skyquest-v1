#pragma once

#include "CoreMinimal.h"
#include "Core/SQGameInstance.h"
#include "ProceduralMeshComponent/Public/ProceduralMeshComponent.h"
#include "GameFramework/Actor.h"
#include "Structs/SQBlockToModify.h"
#include "Structs/SQCompressedBlocks.h"
#include "Structs/SQMask.h"
#include "Usable/SQUtilityBlock.h"
#include "SQChunk.generated.h"

class ASQWorldManager;
UCLASS()
class SKYQUEST_API ASQChunk : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY()
	USQGameInstance* GameInstance;

	const TArray<FIntPoint> TowersChunks = {
		FIntPoint(-2,-2),
		FIntPoint(2,-2),
		FIntPoint(-2,2),
		FIntPoint(2,2)
	};
	
	int32 VertexCount = 0;

	FIntVector ChunkSize = FIntVector(16,16,256);

	virtual void OnRep_Owner() override;
	
	UPROPERTY()
	UCurveFloat* PerlinCurve;
	
	ASQChunk();
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	int32 GetBlockIndex(const int X, const int Y, const int Z) const;

	TMap<int32,int32> HealthBlocks;

	virtual void BeginPlay() override;

	UPROPERTY()
	ASQWorldManager* WorldManager;

	UPROPERTY(Replicated)
	FIntPoint ChunkPos;

	TArray<FVector3d> Vertices;
	TArray<int32> Triangles;
	TArray<FVector3d> Normals;
	TArray<FVector2D> UV;
	TArray<FColor> VertexColors;
	TArray<FProcMeshTangent> Tangents;

	UPROPERTY()
	UMaterial* ChunkMat;

	void CreateQuad(const FSQMask Mask,const FIntVector& AxisMask,const FIntVector& V1,const FIntVector& V2,
		const FIntVector& V3,const FIntVector& V4);

	UFUNCTION(BlueprintCallable)
	int32 GetBlock(const FIntVector Index) const;

	static bool CompareMask(const FSQMask M1, const FSQMask M2);
	
	void PopulateChunk();

	UPROPERTY()
	UProceduralMeshComponent* Mesh;

	void CreateMeshData();

	void RenderChunk();

	void IsAir();

	void ShowChunk() const;

	void HideChunk() const;

	void ModifyBlock(const FIntVector& BlockPos, const int32 NewBlock, int32& OldBlock);

	bool DamageBlock(const FIntVector& BlockPos, const int32 Damage, int32& BlockDestroyed);

	void SpawnStructure(FIntVector ThreePos, const int32 StructureID) const;

	bool OutOfChunk(const FIntVector& BlockPos) const;

	void AddQueueToModify(TArray<FSQBlockToModify> BlockToModifies);

	void AddBlocksToModify();

	void CompressChunk();

	void DecompressChunk();

	UFUNCTION(NetMulticast,Unreliable)
	void Multicast_ChunkUpdate();

	UPROPERTY(BlueprintReadWrite)
	TArray<FSQCompressedBlocks> CompressedChunk;

	TQueue<FSQBlockToModify> BlocksToModify;

	TQueue<FSQBlockToModify> NewUtilityBlocks;

	TArray<int32> BlockList;

	UPROPERTY()
	TMap<FIntVector,ASQUtilityBlock*> UtilitiesBlocks;

	bool bIsGenerated = false;
	
	TArray<FSQMask> Masks;
};
