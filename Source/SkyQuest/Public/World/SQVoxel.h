// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SQVoxel.generated.h"

/**
 * 
 */
UCLASS()
class SKYQUEST_API USQVoxel : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable,BlueprintPure)
	static FIntPoint GetChunkIndices(FVector WorldLocation);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	static FVector TruncToBlock(FVector WorldLocation);

	UFUNCTION(BlueprintCallable,BlueprintPure)
	static int32 GetBlockIndex(FIntVector ChunkSize,FIntVector BlockIndex);

	UFUNCTION(BlueprintCallable,BlueprintPure)
	static FVector GetChunkWorldLocation(FIntVector ChunkSize,FIntPoint ChunkPos);
};
