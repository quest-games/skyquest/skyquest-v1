#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Structs/SQBlockToModify.h"
#include "Structs/SQSavedChunk.h"
#include "SQWorldManager.generated.h"

class ASQChunk;
class ASQPlayerController;

UCLASS()
class SKYQUEST_API ASQWorldManager : public AActor
{
	GENERATED_BODY()
	
public:	
	ASQWorldManager();

protected:
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere)
	UCurveFloat* PerlinCurve;
	
public:
	const TArray<FIntPoint> TowersChunks = {
		FIntPoint(-2,-2),
		FIntPoint(2,-2),
		FIntPoint(-2,2),
		FIntPoint(2,2)
	};

	TQueue<ASQChunk*,EQueueMode::Mpsc> ChunkToUpdate;
	
	virtual void Tick(float DeltaTime) override;

	void SpawnBaseWorld();

	UPROPERTY()
	TMap<FIntPoint, FSQSavedChunk> SavedChunks;

	UFUNCTION(BlueprintCallable)
	bool ChangeBlock(FVector WorldLocation, int32 NewBlock, int32& OldBlock);

	UFUNCTION(BlueprintCallable)
	bool DamageBlock(FVector WorldLocation, int32 Damage, int32& BlockDestroyed);

	UFUNCTION(BlueprintCallable,BlueprintPure)
	int32 GetBlock(FVector WorldLocation, ASQChunk*& Chunk, FIntVector& BlockIndex);

	void AddBlockToModify(FVector WorldLocation, FSQBlockToModify BlockToModify);

	FSQSavedChunk GetChunkData(FIntPoint ChunkPos);

	UFUNCTION(Server,Reliable)
	void Server_PopulateChunks(const TArray<FIntPoint>& Chunks, ASQPlayerController* PlayerController);

	static TArray<FSQCompressedBlocks> CompressChunk(const TArray<int32>& Blocks);

	static TArray<int32> DecompressChunk(const TArray<FSQCompressedBlocks>& CompressedBlocks);
	
	void PopulateChunk(const FIntPoint ChunkPos,TArray<int32>& Blocks, TArray<FSQBlockToModify>& BlocksToModify);
	
	void SpawnStructure(FIntVector ThreePos, FIntPoint ChunkPos, const int32 StructureID);

	void AddBlocksToModify(TArray<int32>& Blocks,TArray<FSQBlockToModify>& BlocksToModify) const;
	
	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	FIntVector ChunkSize = FIntVector(16,16,256);	
};
